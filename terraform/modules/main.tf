#Creating VPC
resource "aws_vpc" "main" {
  cidr_block = var.cidr
  enable_dns_support   = true
  instance_tenancy = "default"
  tags = {
        Name = var.vpc_name
    }
}

# For availability zones
data "aws_availability_zones" "available" {
  state = "available"
}

#Creating Public Subnet 
resource "aws_subnet" "public_Subnet" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.public_subnets
  map_public_ip_on_launch = true
  availability_zone       = data.aws_availability_zones.available.names[0]
  tags = {
   Name                   = var.public_subnets_name
    }
  depends_on    = [aws_vpc.main]
}


#Creating Private Subnets
resource "aws_subnet" "private_Subnet" {
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.private_subnets
  map_public_ip_on_launch = false
  availability_zone       = data.aws_availability_zones.available.names[1]
  tags = {
   Name                   = var.private_subnets_name
   }
  depends_on    = [aws_vpc.main] 
}

# Creating Internet Gateway
resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.main.id
  depends_on    = [aws_vpc.main]
}

# Creating EIP for NAT
resource "aws_eip" "eip_nat" {
  vpc = true
  depends_on               = [aws_vpc.main]
}

# Creating NAT Gateway
resource "aws_nat_gateway" "nat" {
 # count                   =  1
  allocation_id            = aws_eip.eip_nat.id
  subnet_id                = aws_subnet.public_Subnet.id
  depends_on               = [aws_eip.eip_nat]
}

# Creating Public Route Table
resource "aws_route_table" "public_RT" {
  vpc_id                   = aws_vpc.main.id
  depends_on               = [aws_internet_gateway.gateway]
  tags = {
    Name = var.publicRT
    }
}

# Associate Route Table with Public Subnet
resource "aws_route_table_association" "public_association" {
  subnet_id                = aws_subnet.public_Subnet.id
  route_table_id          = aws_route_table.public_RT.id
  depends_on              = [aws_route_table.public_RT]
}

# Crate Traffic Route for Route Table
resource "aws_route" "public" {
  route_table_id          = aws_route_table.public_RT.id
  destination_cidr_block  = "0.0.0.0/0"
  gateway_id              = aws_internet_gateway.gateway.id
  depends_on              = [aws_route_table.public_RT]
}

# Creating Private Route Table
resource "aws_route_table" "private_RT" {
    vpc_id                = aws_vpc.main.id
    depends_on            = [aws_nat_gateway.nat]
      tags = {
    Name = var.privateRT
    }
}

# Associate Route Table with Private Subnet
resource "aws_route_table_association" "private_association" {
  subnet_id               = aws_subnet.private_Subnet.id
  route_table_id          = aws_route_table.private_RT.id
  depends_on              = [aws_route_table.private_RT]

}

# Creating Private Route Table
resource "aws_route" "private" {
  route_table_id         = aws_route_table.private_RT.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat.id
  depends_on             = [aws_route_table.private_RT]
}
