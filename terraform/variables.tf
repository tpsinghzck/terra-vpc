variable "vpc_name" {
  description = "AWS VPC Name"
  type = string
  }

variable "region" {
  description = "AWS Region Name"
  type = string
  }

variable "cidr" {
  description = "AWS VPC CIDR Range"
  type = string
  }

variable "public_subnets_name" {
  description = "AWS Public Subnets  name"
  type = string
  }

variable "public_subnets" {
  description = "AWS Public Subnets CIDR Range"
  type = string
  }

variable "private_subnets_name" {
  description = "AWS Private Subnets name"
  type = string
  }

variable "private_subnets" {
  description = "AWS Public Subnets CIDR Range"
  type = string
  }


variable "publicRT" {
  description = "AWS Public Route Table name"
  type = string
  }

variable "privateRT" {
  description = "AWS Private Route Table name"
  type = string
  }