terraform {
  backend "s3" {
    bucket = "jammutaran"
    key    = "terraform.tfstate"
    region = "ap-south-1"
  }
}