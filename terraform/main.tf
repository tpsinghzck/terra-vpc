#terraform apply -var-file=env/environment.tfvars 


provider "aws" {
  region                  =  var.region
  shared_credentials_file = "/home/taran/.aws/credentials"
  profile                 = "default"
}


module "vpc-aws" {
  source = "./modules"
  vpc_name              = var.vpc_name
  cidr                  = var.cidr
  region                = var.region
  public_subnets_name   = var.public_subnets_name
  public_subnets        = var.public_subnets
  private_subnets_name  = var.private_subnets_name
  private_subnets       = var.private_subnets
  #zones                = var.zones
  publicRT              = var.publicRT
  privateRT             = var.privateRT

  # vpc_name              = "${var.vpc_name}"
  # cidr                  = "${var.cidr}"
  # region                = "${var.region}"
  # public_subnets_name   = "${var.public_subnets_name}"
  # public_subnets        = "${var.public_subnets}"
  # private_subnets_name  = "${var.private_subnets_name}"
  # private_subnets       = "${var.private_subnets}"
  # #zones                = "${var.zones}"
  # publicRT              = "${var.publicRT}"
  # privateRT             = "${var.privateRT}"
}


